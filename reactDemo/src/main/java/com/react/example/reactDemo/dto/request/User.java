package com.react.example.reactDemo.dto.request;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel
public class User {
	@ApiModelProperty(example="sunil",required=true)
	private String userFirstName;
	@ApiModelProperty(example="behera",required=true)
	private String userLastName;
	@ApiModelProperty(example="sunil@incedoinc.com",required=true)
	private String userEmail;
	@ApiModelProperty(example="999999999",required=true)
	private String userMobile;
	/*@ApiModelProperty(example="21-04-1995",required=true)
	private Date userDob;*/


	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserLastName() {
		return userLastName;
	}
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserMobile() {
		return userMobile;
	}
	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}
	/*public Date getUserDob() {
		return userDob;
	}
	public void setUserDob(Date userDob) {
		this.userDob = userDob;
	}*/
	@Override
	public String toString() {
		return "User [userFirstName=" + userFirstName + ", userLastName=" + userLastName + ", userEmail=" + userEmail
				+ ", userMobile=" + userMobile + "]";
	}

}
