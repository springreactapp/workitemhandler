package com.react.example.reactDemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.react.example.reactDemo.dao.UserDao;
import com.react.example.reactDemo.dto.request.User;
import com.react.example.reactDemo.dto.response.Message;
import com.react.example.reactDemo.entity.UserEntity;
@Service
public class UserServiceImpl implements UserService {
	
@Autowired
	private UserDao userDao;
	@Override
	public Message saveUser(UserEntity user) {
		userDao.save(user);
		Message message = new Message();
		
		
		return  null;
	}
	@Override
	public Iterable<UserEntity> listAllUsers() {
		return userDao.findAll();
	}
	@Override
	public UserEntity getUserById(Integer id) {
		return userDao.findById(id).get();
	}

	public String getSequenceNumber(){
		
		
		return null;
		
	}

}
