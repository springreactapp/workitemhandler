import React, { Component } from "react";
import Axious from "axios";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userFirstName: "",
      userLastName: "",
      userEmail: "",
      course: "spring"
    };
  }

  handleUserNameChange = event => {
    console.log(event.target.value);
    this.setState({
      userFirstName: event.target.value
    });
  };

  handleUserLastNamee = event => {
    this.setState({
      userLastName: event.target.value
    });
  };

  handleEmail = event => {
    this.setState({
      userEmail: event.target.value
    });
  };
  handletopichange = event => {
    this.setState({
      course: event.target.value
    });
  };

  formSubmitChange = event => {
    event.preventDefault();
    console.log(this.state);
    Axious.post("http://localhost:8082/saveUser", this.state)
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    return (
      <form onSubmit={this.formSubmitChange}>
        <div>
          <label>FirstName</label>
          <input
            type="text"
            value={this.state.userFirstName}
            onChange={this.handleUserNameChange}
          />
        </div>
        <br></br>
        <div>
          <label>LastName</label>
          <input
            type="text"
            value={this.state.userLastName}
            onChange={this.handleUserLastNamee}
          />
        </div>
        <br></br>
        <div>
          <label>Email</label>
          <input
            type="text"
            value={this.state.userEmail}
            onChange={this.handleEmail}
          />
        </div>
        <br></br>
        <div>
          <select value={this.state.course} onChange={this.handletopichange}>
            <option value="spring">Spring</option>
            <option value="microservice">MicroService</option>
            <option value="reactjs">React Js</option>
          </select>
        </div>
        <br />
        <div>
          <button type="submit">Submit</button>
          <button type="reset">Clear</button>
        </div>
      </form>
    );
  }
}

export default Form;
